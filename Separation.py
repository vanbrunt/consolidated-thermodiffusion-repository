from firedrake import *
import numpy as np
import math as m
from mpi4py import MPI
from matplotlib import pyplot as plt

Citations.print_at_exit()

print_ = print
def print(*args, **kwargs):
    kwargs["flush"] = True
    if MPI.COMM_WORLD.rank == 0:
        print_(*args, **kwargs)

##################################
#Define the physical parameters
##################################

n = 3   # Number of species.
Rgas = 8.31446261815324  # Exact Ideal gas constant (J mol^{-1} K^{-1})
Tref = 300   # Reference temperature (K)
# We will enumerate the species as follows:
# 0 is Neon
# 1 is Argon
# 2 is Krypton

# Now we define the matrix of diffusion coefficents; units are in cm^{2}/s
Diffcoefmatrix = [[0 for i in range(0,n)] for j in range(0,n)]
Diffcoefmatrix[0][1] = .7560
Diffcoefmatrix[1][0] = Diffcoefmatrix[0][1]
Diffcoefmatrix[0][2] = .6653
Diffcoefmatrix[2][0] = Diffcoefmatrix[0][2]
Diffcoefmatrix[1][2] =  0.2647
Diffcoefmatrix[2][1] = Diffcoefmatrix[1][2]

# Now define the antisymmetric matrix A_ij; units are in cm^{2}.s
Athermatrix = [[0 for i in range(0,n)] for j in range(0,n)]
Athermatrix[0][1] = -0.2910
Athermatrix[1][0] = -Athermatrix[0][1]
Athermatrix[0][2] =  -0.2906
Athermatrix[2][0] = -Athermatrix[0][2]
Athermatrix[1][2] =  0.0004
Athermatrix[2][1] = -Athermatrix[1][2]

# Define the thermal conductivity of each of the pure components; units are in W/(cm.K)
Thermconduct = [0 for i in range(0,n)]
Thermconduct[0] = 15.66
Thermconduct[1] =  1.78
Thermconduct[2] = .95

# Define the molar mass of each species; units are in g/mol
M = [0 for i in range(0,n)]
M[0] = 4.00
M[1] = 39.95
M[2] = 83.80
pref = 101325/(100*100*100) # reference atomspheric pressure (J/cm^{3})
cTotal = pref/(Rgas*Tref)

# Define the influx density in W/cm^{2}. Scaled by pi for convenience
Influx = Constant(15/3.14)
# Note we recover the total Watts entering the system by 15*3*3 = 135

###############################
#Define the function spaces
##############################

# Import mesh of seperation chamber
mesh = Mesh("Separation.msh")

# Function space for concentration and temperature
k = 1
X = FunctionSpace(mesh, "CG", k)

# Function space for species velocity
Q = VectorFunctionSpace(mesh, "DG", k-1)

# Final function space
W = MixedFunctionSpace([FunctionSpace(mesh, "CG", 1) for i in range(n+1)] + [VectorFunctionSpace(mesh, "DG", 0) for i in range(n+1)])

U_n = Function(W)
U = TrialFunction(W)
UU = TestFunction(W)

# Compute degrees of freedom to see size of the problem.
print(f"Total degrees of freedom: {W.dim()}")

# Unpack the function space to define the trial and test functions.
# x_{i} is the mole fraction of the ith species, v_{i} is velocity of ith species.
# T is temperature, v0 is the irreversible heat flux divided by the reference pressure.
# This turns out to be a convenient way to scale the problem.

x1, x2, x3, T, v1, v2, v3, v0 = split(U)

# Split the test functions
w1, w2, w3, w4, tau1, tau2, tau3, tau0  = split(UU)

# Wrap these functions into a list
tildex = [x1, x2, x3, T]
tildev = [v1, v2, v3, v0]
tildew = [w1, w2, w3, w4]
tildetau = [tau1, tau2, tau3, tau0]

# Finally compute the volume of the mesh, we will use this later.
Vol = assemble(Constant(1)*dx(mesh))

##########################
#Set the boundary data
##########################

# Initialise the Dirichlet boundary list
DBClist = []
# Set the BC at the one end of the separation chamber
DBClist.append(DirichletBC(W.sub(3), Constant(300), (4,2)))

# Need to define a nullspace to impose conservation of the total number of moles.
nullspace = MixedVectorSpaceBasis(
      W, [VectorSpaceBasis(constant = True), VectorSpaceBasis(constant = True), VectorSpaceBasis(constant = True), W.sub(3), W.sub(4), W.sub(5), W.sub(6), W.sub(7)])

#############################
#Initialise quanties and define some bilinear forms
#############################

# Firsly we have to set up the initial guess. We take the initial guess as
prevx = [Function(X).interpolate(Constant(1/n)) for i in range(0,n)]
prevT = Function(X).interpolate(Constant(Tref))
# The total concentration must be computed dynamically. We initialise it according the the equation of state.
# The average of this will not change, will will use it later when updating the pressure.
prevcTotal = Function(X).interpolate(pref/(Rgas*prevT))
pnew = Constant(pref)
# We don't need this for the solver, but use it to check the residual
prevv = [Function(Q).interpolate(as_vector([0,0,0])) for i in range(0,n+1)]


bck = 0  # This is the bilinear form associated to the steady state continuity equation.
R = 0  # This is the linear functional which has the reaction rates
for i in range(0,n):
  bck += (dot(grad(tildew[i]), prevcTotal*prevx[i]*tildev[i]))*dx
  R += Constant(0)*tildew[i]*dx
bck += pref*dot(grad(tildew[n]), tildev[n])*dx
R += -Influx*tildew[n]*ds(1)
gamma =1 # The penalty parameter in the augmented approach.

##########################################
#Isothermal part of the transport problem.
##########################################

# Initialise the isothermal transport matrix
Mtrans = [[0 for i in range(0,n)] for j in range(0,n)]
# Define the isothermal transport matrix
for i in range(0,n):
    for j in range(0,n):
        if (i !=j):
            Mtrans[i][j] = -(pref*prevx[i]*prevx[j]/(pnew*Diffcoefmatrix[i][j]))  # Note: units are cm^{2}/s

for i in range(0,n):
    for j in range(0,n):
        if (i !=j):
            Mtrans[i][i] += -Mtrans[i][j]

# We will use this soon for the augmentation
density = 0
prevmass = as_vector([0,0,0])
for i in range(0,n):
  density += prevx[i]*M[i]
  prevmass += M[i]*prevx[i]*prevv[i]/density

#########################################
#Define the thermal aspect of the problem
#########################################

# Define the long-winded mixing rule for thermal conductivity.
kmix = (1/2)*(Thermconduct[0]*Thermconduct[1]*Thermconduct[2])/(prevx[0]*Thermconduct[1]*Thermconduct[2] +prevx[1]*Thermconduct[0]*Thermconduct[2]+prevx[2]*Thermconduct[0]*Thermconduct[1])
kmix += (1/2)*(prevx[0]*Thermconduct[0]+prevx[2]*Thermconduct[1]+prevx[2]*Thermconduct[2])

# Initialise the non-isothermal transport matrix
Mthermtrans = [[0 for i in range(0,n+1)] for j in range(0,n+1)]
# The M_00 entry in the transport matrix.
Mthermtrans[n][n] =  pref/(kmix*prevT)      #J/cm^3 * cm /W = s/cm^{2}

# Define the M_0i entries of the isothermal transport matrix. We initialise first
Mtranscolumn = [0 for i in range(0,n)]
for i in range(0,n):
  for j in range(0,n):
    if (i != j):
      Mtranscolumn[i] += Mtrans[i][j]*(pnew/pref)*(Athermatrix[i][j])*Mthermtrans[n][n] #s/cm^{2}

  # Now set the off-diagonal blocks
  Mthermtrans[n][i] = Mtranscolumn[i]
  Mthermtrans[i][n] = Mtranscolumn[i]

# Set the main block of the transport matrix
for i in range(0,n):
  for j in range(0,n):
    Mthermtrans[i][j] = Mtrans[i][j] - (Mtranscolumn[i]*Mtranscolumn[j])/Mthermtrans[n][n]

# Finally add the augmentation
for i in range(0,n):
    for j in range(0,n):
        Mthermtrans[i][j] += gamma*(prevx[i]*prevx[j]*M[i]*M[j]/density)

########################
#Define the remaining bilinear forms
########################
b = 0  # Bilinear form for concentration gradient
A = 0  # Symmetric coercive bilinear form on Q*Q

for i in range(0, n):
    b += (dot(grad(tildex[i]), tildetau[i])) * dx

b += (1/prevT)*dot(grad(T), tildetau[n])*dx
for i in range(0, n+1):
    for j in range(0, n+1):
        A += Mthermtrans[i][j]*dot(tildev[i], tildetau[j])*dx


##########################################
#Assemble the solver and specify option
##########################################

#The summed bilinear forms
Fsolv = bck + A + b
L = R

# Specify the solver to solve directly using a mumps package
sp ={"mat_type": "aij",
     "ksp_type": "preonly",
     "pc_type": "lu",
     "pc_factor_mat_solver_type": "mumps",
     "ksp_converged_reason": None,
     "mat_mumps_icntl_14": 800}

# We define the residual function.
def Residual(U, prevc, prevv, prevT):
  red = 0
  redxH1 = 0
  redTH1 = 0
  redv = 0
  for i in range(0, n):
    #red += sqrt(assemble(dot(U[i]- prevc[i],U[i]- prevc[i])*dx))
    print(sqrt(assemble(dot(grad(U[i])- grad(prevc[i]), grad(U[i])- grad(prevc[i]))*dx)))
    redxH1 += sqrt(assemble(dot(grad(U[i])- grad(prevc[i]), grad(U[i])- grad(prevc[i]))*dx))
    redv += sqrt(assemble(dot(U[i+n+1]- prevv[i],U[i+n+1]- prevv[i])*dx))
  red += sqrt(assemble(dot(U[n]- prevT,U[n]- prevT)*dx))
  redTH1 += sqrt(assemble(dot(grad(U[n]-prevT), grad(U[n]-prevT))*dx))
  red += redxH1 + redTH1 + redv
  return red



##############################
#Solve the nonlinear system
##############################
# Useful to define an averages list
Averages = [1 for i in range(0,n)]

# Now we start the outer solve
Runningred = 1  # Define the running residual
counter = 1     # A counter of the number of linear solves
TOL = 10e-10    # The tolerance level

# Define the output files.
outfileHe = File("SeparationHe.pvd")
outfileAr = File("SeparationAr.pvd")
outfileKr = File("SeparationKr.pvd")
outfileT = File("SeparationT.pvd")
outfileHeV = File("SeparationHeV.pvd")
outfileArV = File("SeparationArV.pvd")
outfileKrV = File("SeparationKrV.pvd")
outfileTQ = File("SeparationTQ.pvd")
outfileEnt = File("SeparationEnt.pvd")

while (Runningred > TOL):
  # Solve the linear system
  solve(Fsolv==R, U_n, bcs=DBClist, solver_parameters=sp, nullspace = nullspace)
  U = split(U_n)

  # Calculate and print the residual
  Runningred = Residual(U, prevx, prevv, prevT)
  print(f"Running residual is: {Runningred}")

  # Set the averages for each of the mole fraction. It is usefl to print them for debugging purposes
  for i in range(0,n):
    Averages[i] = assemble(U[i]*dx)/Vol
    print(f"Average {i}: {Averages[i]}")

  # Update the mole fraction for the next iteration. Note we recenter the mole fractions
  for i in range(0,n):
    prevx[i].assign(Function(X).interpolate(U[i])-Averages[i]+1/3)
    prevv[i].assign(Function(Q).interpolate(U[i+n+1]))

  # Check the gradient of the total mole fraction. Should be very small
  print(f"Gradient of mole fraction: {sqrt(assemble(dot(grad(U[0]+U[1]+U[2]), grad(U[0]+U[1]+U[2]))*dx))}")

  # Update the heat flux
  prevT.assign(Function(X).interpolate(U[n]))

  # Update pressure
  pnew.assign(Constant(cTotal*Vol/((assemble((1/(Rgas*U[n]))*dx)))))
  print(f"The new pressure is: {assemble(Function(X).interpolate(Constant(pnew))*dx)/Vol}")

  # Update cT and the velocities.
  prevcTotal.assign(Function(X).interpolate(pnew/Rgas*U[n]))
  prevv[n].assign(Function(Q).interpolate(U[2*n]))


  # Compute the Gibbs--Duhem residual and the mass average velocity of the converged solution
  calculatedmass_flux =as_vector([0,0,0])
  xTotal = 0
  for i in range(0,n):
    calculatedmass_flux += prevx[i]*U[i+n+1]
    xTotal += U[i]-Averages[i]+1/3

  # Print the errors on the Gibbs--Duhem equation and the mass-average velocity constraint.
  print(f"Gibbs Duhem relation (scaled by the volume of the mesh): {sqrt(assemble(dot(xTotal-Constant(1), xTotal-Constant(1))*dx))/Vol}")
  print(f"Mass average velocity (scaled by the volume of the mesh): {sqrt(assemble(dot(calculatedmass_flux, calculatedmass_flux)*dx))/Vol}")

  counter += 1
  # End  of outer solve loop

######################
#Analyse the output
######################

# Print the number of non-linear iterations required for convergence
print(f"Number of non-linear solves: {counter}")
#Calculate the Entropy production out of interest
Entprod = 0
for i in range(0,n):
    Entprod += Rgas*prevcTotal*dot(U[n+i+1],grad(U[i]))+(kmix)*(1/(prevT*prevT))*dot(grad(U[3]), grad(U[3])) #J K^-1 cm^-3 s^-1

# Export data for analysis
outfileEnt.write(project(Entprod, X, name="Entropy production"))
outfileHe.write(project(U[0]-Averages[0]+1/3, X, name="SeparationHe"))
outfileAr.write(project(U[1]-Averages[1]+1/3, X, name="SeparationAr"))
outfileKr.write(project(U[2]-Averages[2]+1/3, X, name="SeparationKr"))
outfileT.write(project(U[3], X, name="SeparationT"))

outfileHeV.write(project(U[4], Q, name="SeparationHeV"))
outfileArV.write(project(U[5], Q, name="SeparationArV"))
outfileKrV.write(project(U[6], Q, name="SeparationKrV"))
# The last one we scale by pref to get the irreversible heat flux.
outfileTQ.write(project(U[7]*pref, Q, name="SeparationTQ"))
######################################################################

